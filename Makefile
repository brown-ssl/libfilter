.PHONY: all regen deps clean
all: deps
	$(MAKE) -C app
deps:
	$(MAKE) -C extraction
	$(MAKE) -C eraser
clean:
	$(MAKE) -C app clean
	$(MAKE) -C extraction clean
	$(MAKE) -C eraser clean
