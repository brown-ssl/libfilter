#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

WORKING_DIR=$PWD
TEST_SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
TEST_DIR="$TEST_SCRIPT_DIR/.."
LIBC_DIR="$TEST_DIR/glibc"
LIBC_BUILD_DIR="$LIBC_DIR/build"
LIBC_ELF_DIR="$LIBC_BUILD_DIR/elf"

LIBC_ELF_TESTS=(
  "$LIBC_ELF_DIR/resolvfail"
  "$LIBC_ELF_DIR/reldep"
  "$LIBC_ELF_DIR/reldep2"
  "$LIBC_ELF_DIR/reldep3"
  "$LIBC_ELF_DIR/reldep4"
  "$LIBC_ELF_DIR/nodelete2"
  "$LIBC_ELF_DIR/initfirst"
  "$LIBC_ELF_DIR/restest2"
  "$LIBC_ELF_DIR/dblload"
  "$LIBC_ELF_DIR/dblunload"
  "$LIBC_ELF_DIR/reldep6"
  "$LIBC_ELF_DIR/reldep8"
  "$LIBC_ELF_DIR/tst-tls10"
  "$LIBC_ELF_DIR/tst-tls11"
  "$LIBC_ELF_DIR/tst-tls12"
  "$LIBC_ELF_DIR/tst-tls15"
  "$LIBC_ELF_DIR/tst-tls16"
  "$LIBC_ELF_DIR/tst-tls17"
  "$LIBC_ELF_DIR/tst-tls18"
  "$LIBC_ELF_DIR/tst-dlopenrpath"
  "$LIBC_ELF_DIR/tst-dlmopen1"
  "$LIBC_ELF_DIR/tst-dlmopen3"
  "$LIBC_ELF_DIR/unload6"
  "$LIBC_ELF_DIR/unload7"
  "$LIBC_ELF_DIR/unload8"
  "$LIBC_ELF_DIR/tst-audit2"
  "$LIBC_ELF_DIR/tst-audit9"
  "$LIBC_ELF_DIR/tst-thrlock"
  "$LIBC_ELF_DIR/tst-unique1"
  "$LIBC_ELF_DIR/tst-unique2"
  "$LIBC_ELF_DIR/tst-relsort1"
  "$LIBC_ELF_DIR/tst-null-argv"
  "$LIBC_ELF_DIR/tst-dlsym-error"
  "$LIBC_ELF_DIR/tst-noload"
  "$LIBC_ELF_DIR/tst-tls-manydynamic"
  "$LIBC_ELF_DIR/tst-debug1"
  "$LIBC_ELF_DIR/tst-main1"
  "$LIBC_ELF_DIR/tst-big-note"
)

# Check environment variables for a path to the libfilter tool
LIBFILTER_EXEC_DIR=${LIBFILTER_EXEC_DIR:-"$TEST_DIR/../app/"}

# Get and build libc
# Note: If this script is run multiple times, cloning will report a "fatal error".
# Ignoring this error is fine, since after the first run the clone will already exist anyway.
git clone https://sourceware.org/git/glibc.git "$LIBC_DIR"
cd $LIBC_DIR
git checkout release/2.27/master
mkdir build
cd build
./../configure --prefix=$LIBC_BUILD_DIR --disable-werror
echo "Building glibc..."
make -s --no-print-directory -j8
echo "Building glibc tests..."
make check -s --no-print-directory -j8
cd $WORKING_DIR

TESTS_RUN=0
TESTS_PASSED=0
TESTS_FAILED=''
for ((i=0; i<${#LIBC_ELF_TESTS[@]}; i++))
do
  _test_exec=${LIBC_ELF_TESTS[$i]}
  _test_exec_out="${LIBC_ELF_TESTS[$i]}.out"
  _test_lib_out="$(basename $_test_exec).libs"
  _test_json="$_test_exec.json"

  if $($LIBC_BUILD_DIR/testrun.sh $_test_exec); then
    ((TESTS_RUN++))
    $LIBFILTER_EXEC_DIR/libfilter_main -j $_test_json -x $_test_exec_out -l $_test_lib_out $_test_exec
    if [ $? -ne 0 ]; then
      TESTS_FAILED+=$_test_exec
      TESTS_FAILED+='\n'
    elif [ $($LIBC_BUILD_DIR/testrun.sh $_test_exec_out) -ne 0 ]; then
      TESTS_FAILED+=$_test_exec
      TESTS_FAILED+='\n'
    else
      ((TESTS_PASSED++))
    fi
  fi
done

# Try filtering libc itself
((TESTS_RUN++))
libc_library="$LIBC_BUILD_DIR/libc.so.6"
libc_json="$_test_exec.json"
$LIBFILTER_EXEC_DIR/libfilter_main -j $libc_json $libc_library
if [ $? -ne 0 ]; then
    TESTS_FAILED+=$libc_json
    TESTS_FAILED+='\n'
else
    ((TESTS_PASSED++))
fi

echo "libc_elf: $TESTS_PASSED of $TESTS_RUN tests passed"
if [ ! -z "${TESTS_FAILED}" ]; then
    echo -e "${RED}Tests failed:${NC}"
    echo -e $TESTS_FAILED
    exit 1
else
    exit 0
fi
