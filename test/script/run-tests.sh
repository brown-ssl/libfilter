#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

WORKING_DIR=$PWD
TEST_SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
LIBFILTER_ROOT="$TEST_SCRIPT_DIR/../.."
TESTS_PASSED=0

TEST_SET=${TEST_SET:-"base"}

test_scripts=(
    "$TEST_SCRIPT_DIR/hello_world.sh"
)

if [ $TEST_SET == "extended" ]; then
    test_scripts+=("$TEST_SCRIPT_DIR/libc_elf.sh")
fi

failed_tests=''
for ((i=0; i<${#test_scripts[@]}; i++))
do
    _test=${test_scripts[$i]}
    echo $_test
    if $_test; then
	    ((TESTS_PASSED++))
    else
        failed_tests+=$_test
        failed_tests+='\n'
    fi
done

# Generate code coverage report
cd $LIBFILTER_ROOT/app/
gcov build_x86_64/libfilter_main
lcov --capture --base-directory . --directory . --output-file verbose.info
lcov --remove verbose.info -o coverage.info \
    "/usr/*" \
    "*/sysfilter/*"
genhtml coverage.info --output-directory $LIBFILTER_ROOT/html
cd $WORKING_DIR

echo -e "${GREEN}${TESTS_PASSED} tests passed out of ${#test_scripts[@]}${NC}"
if [ ! -z "${failed_tests}" ]; then
    echo -e "${RED}Tests failed:${NC}"
    echo -e $failed_tests
    exit 1
else
    exit 0
fi
