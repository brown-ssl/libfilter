import os
import sys
import subprocess
import json
import argparse

HALT_INSTRUCTION_TAG = "hlt"
NOOP_INSTRUCTION_TAG = "nop"
FAIL_COLOR = '\033[91m'
DEFAULT_COLOR = '\033[0m'

MODULE_PREFIX = "module-"
PROTOBUF_SUFFIX = ".disasm"

KEY_BINARY_NAME = "binName"
KEY_MODULE_LIST = "libs"
KEY_MODULE_NAME = "libName"
KEY_FUNCTION_LIST = "funcs"
KEY_FUNCTION_NAME = "funcName"
KEY_FUNCTION_ADDRESS = "offset"
KEY_FUNCTION_SIZE = "size"
KEY_FUNCTION_ERASED = "erased"

KEY_EXCLUDED_LIBS = ":libs:"

# Parses exclude data into a dictionary that maps library names to excluded functions
# If a whole library is marked for exclusion, it is added under KEY_EXCLUDED_LIBS
def parse_exclude_data(exclude_path):
    exclude_file = open(exclude_path)
    exclude_lines = exclude_file.readlines()
    exclude_file.close()

    exclude_data = { KEY_EXCLUDED_LIBS : [] }
    for entry in exclude_lines:
        entry = entry.split(':')
        lib_name = entry[0]
        if (len(entry) < 2):
            exclude_data[KEY_EXCLUDED_LIBS].append(lib_name.strip())
        elif (lib_name in exclude_data):
            exclude_data[lib_name].append(entry[1].strip())
        else:
            exclude_data[lib_name] = [ entry[1].strip() ]

    return exclude_data

def get_library_paths(exec_path):
    ldd_args = ["ldd", exec_path]
    ldd_out = str(subprocess.check_output(ldd_args))
    libraries = ldd_out.split('\\n\\t')
    library_paths = []
    for entry in libraries:
        entry = entry.split(" ")
        if (len(entry) > 3):
            library_paths.append(entry[2])
    return library_paths

# Parses JSON erase data into a dictionary that maps module names to function lists
def format_erase_data(erase_data):
    # Format the entry for the executable
    exec_module = erase_data[KEY_MODULE_LIST][0]
    exec_module[KEY_MODULE_NAME] = MODULE_PREFIX + erase_data[KEY_BINARY_NAME]

    erase_dict = {}
    for module in erase_data[KEY_MODULE_LIST]:
        module_name = module[KEY_MODULE_NAME].replace(MODULE_PREFIX, '')
        erase_dict[module_name] = module[KEY_FUNCTION_LIST]

    return erase_dict

# Generates a dictionary that maps addresses to instruction mnemonics
def get_objdump_data(exec_path):
    objdump_args = ["objdump", "-D", exec_path]
    objdump_raw = str(subprocess.check_output(objdump_args))

    instructions = objdump_raw.split('\\n')
    instruction_map = {}
    for instruction in instructions:
        instruction = instruction.split('\\t')
        # objdump instruction entries contain 3 tab-separated sections:
        # Address, instruction bytes, instruction mnemonic
        if len(instruction) == 3:
            instruction_addr_str = instruction[0].strip().replace(':', '')
            instruction_addr = int(instruction_addr_str, 16)
            instruction_value = instruction[2].strip()
            instruction_map[instruction_addr] = instruction_value

    return instruction_map

def function_is_erased(objdump_map, function_erase_data):
    function_start = function_erase_data[KEY_FUNCTION_ADDRESS]
    function_stop = function_start + function_erase_data[KEY_FUNCTION_SIZE]
    for i in range(function_start, function_stop):
        if not i in objdump_map:
            return False
        elif (objdump_map[i] != HALT_INSTRUCTION_TAG) and (objdump_map[i] != NOOP_INSTRUCTION_TAG):
            return False

    return True

def verify_module_erase(exec_path, function_erase_data, exclude_functions, exclude_lib):
    objdump_map = get_objdump_data(exec_path)
    failed_functions = []
    for erase_function in function_erase_data:
        if (erase_function[KEY_FUNCTION_ERASED]):
            function_name = erase_function[KEY_FUNCTION_NAME]
            if exclude_lib or (function_name in exclude_functions):
                failed_functions.append(function_name)
            elif (not function_is_erased(objdump_map, erase_function)):
                failed_functions.append(function_name)

    return failed_functions

def verify_exec_erase(exec_path, exec_erase_data, exclude_data):
    failed_modules = []
    module_key = os.path.basename(exec_path)
    if module_key in exec_erase_data:
        erase_functions = [] if not (module_key in exec_erase_data) else exec_erase_data[module_key]
        exclude_functions = [] if not (module_key in exclude_data) else exclude_data[module_key]
        exclude_lib = module_key in exclude_data[KEY_EXCLUDED_LIBS]
        print("Checking module: " + module_key)
        failed_functions = verify_module_erase(exec_path, erase_functions, exclude_functions, exclude_lib)
        if (len(failed_functions) > 0):
            failed_modules.append({KEY_MODULE_NAME : module_key, KEY_FUNCTION_NAME : failed_functions})

    return failed_modules

def report_failure(failed_modules):
    print("%sErase check failed for the following functions:%s" %\
        (FAIL_COLOR, DEFAULT_COLOR))
    for entry in failed_modules:
        print("Module name: %s" % entry[KEY_MODULE_NAME])
        print("\tFunctions: %s" % str(entry[KEY_FUNCTION_NAME]))

def main():
    parser = argparse.ArgumentParser(description='Verify binary and library erase against an extract json')
    parser.add_argument('-j', dest='erase_path', action='store', required=True,
                        help='json erase output')
    parser.add_argument('-b', dest='exec_path', action='store', required=True,
                        help='erased binary to evaluate')
    parser.add_argument('-e', dest='exclude_path', action='store', required=False,
                        help='Optional exclude file')
    args = parser.parse_args()

    erase_file = open(args.erase_path)
    erase_data = json.load(erase_file)
    erase_file.close()
    erase_data = format_erase_data(erase_data)

    exclude_data = { KEY_EXCLUDED_LIBS : [] }
    if args.exclude_path:
        exclude_data = parse_exclude_data(args.exclude_path)

    libraries = get_library_paths(args.exec_path)

    failed_modules = verify_exec_erase(args.exec_path, erase_data, exclude_data)
    for lib_path in libraries:
        lib_fails = verify_exec_erase(lib_path, erase_data, exclude_data)
        failed_modules = failed_modules + lib_fails

    if (len(failed_modules) > 0):
        report_failure(failed_modules)
        exit(1)
    print("Verification passed")

try:
    main()
except Exception as ex:
    print("%sVerification failed with exception: %s%s" %\
        (FAIL_COLOR, str(ex), DEFAULT_COLOR))
    exit(1)
