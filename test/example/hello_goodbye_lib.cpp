#include "hello_goodbye_lib.h"
#include <iostream>

using namespace std;

void say_hello() {
    cout << "Hello world" << endl;
}

void say_goodbye() {
    cout << "Goodbye world" << endl;
}

void unused_function() {
    cout << "Unused" << endl;
}
