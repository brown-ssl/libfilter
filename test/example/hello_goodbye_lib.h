#ifndef HELLO_GOODBYE_LIB_H
#define HELLO_GOODBYE_LIB_H

#ifdef __cplusplus
extern "C" {
#endif

void say_hello();

void say_goodbye();

void unused_function();

#ifdef __cplusplus
}
#endif

#endif /* HELLO_GOODBYE_LIB_H */
