#include "eraser.h"
#include <stdio.h>

int main(int argc, char **argv) {
    if (argc < 3 || argc > 4) {
        printf("Usage \t./eraser <binary> <byte start> <byte length>\n");
        printf("\t./eraser <binary> <erase file>\n");
        return 1;
    }

    unsigned int byte_start = 0;
    unsigned int byte_len = 0;
    int ret_val = 0;
    if (argc == 4) {
        sscanf(argv[2], "%x", &byte_start);
        sscanf(argv[3], "%x", &byte_len);
        ret_val = erase_from_range(argv[1], byte_start, byte_len);
    }
    else {
        ret_val = erase_from_file(argv[1], argv[2]);
    }

    return ret_val;
}
