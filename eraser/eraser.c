#define _GNU_SOURCE
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

int erase_function(int fd, unsigned int byte_start, unsigned int byte_len) {
    // seek to starting byte
    lseek(fd, byte_start, SEEK_SET);

    char *buf = (char *)malloc(byte_len);
    if (!buf) {
        return 1;
    }
    memset(buf, '\xf4', byte_len);
    unsigned int bytes_written = 0;
    while (bytes_written < byte_len) {
        ssize_t t = write(fd, buf, byte_len - bytes_written);
        if (t == -1) {
            return 1;
        }
        bytes_written += (unsigned int)t;
    }
    free(buf);
    return 0;
}

int erase_from_file(const char *binary_path, const char *erase_path) {
    int binary_fd = open(binary_path, O_RDWR);
    if (binary_fd < 0) {
        printf("Failed to open binary\n");
        return 1;
    }

    unsigned int byte_start = 0;
    int byte_len = 0;
    int ret_val = 0;
    FILE *erase_file = fopen(erase_path, "r");
    if (!erase_file) {
        printf("Failed to open erase_file\n");
        ret_val = 1;
    }
    else {
        char *buf = (char *)malloc(1024 * sizeof(char));
        if (!buf) {
            printf("Failed to allocate line buffer\n");
            ret_val = 1;
        }
        else {
            memset(buf, 0, 1024);
            size_t len = 1024;
            while (getline((char **)&buf, &len, erase_file) != -1) {
                char *start = strtok(buf, " ");
                if (!start) {
                    printf("No first element\n");
                    ret_val = 1;
                    break;
                }
                sscanf(start, "%x", &byte_start);
                char *b_len = strtok(NULL, " ");
                if (!b_len) {
                    printf("No second element\n");
                    ret_val = 1;
                    break;
                }
                sscanf(b_len, "%d", &byte_len);
                if (erase_function(
                        binary_fd, byte_start, (unsigned int)byte_len)) {
                    printf("Erase failed\n");
                    ret_val = 1;
                }
            }
            free(buf);
        }
    }
    fclose(erase_file);
    close(binary_fd);
    return ret_val;
}

int erase_from_range(
    const char *binary_path, unsigned int byte_start, unsigned int byte_len) {
    int binary_fd = open(binary_path, O_RDWR);
    if (binary_fd < 0) {
        printf("Failed to open binary\n");
        return 1;
    }

    int ret_val = 0;
    if (erase_function(binary_fd, byte_start, (unsigned int)byte_len)) {
        printf("Erase failed\n");
        ret_val = 1;
    }
    close(binary_fd);
    return ret_val;
}
