#ifndef ERASED_STATS_H
#define ERASED_STATS_H

#include <string>
#include "static_fcg.h"

class FunctionInfo {
private:
    std::string name;
    size_t size;
    address_t offset;
    bool erased;

public:
    FunctionInfo(Function *func);
    std::string getName() { return name; };
    size_t getSize() { return size; };
    address_t getOffset() { return offset; };
    bool isErased() { return erased; };
    void markAsErased() { erased = true; };
};

class LibraryInfo {
private:
    std::string name;
    std::vector<FunctionInfo *> funcs;

public:
    LibraryInfo(Library *lib);

    std::string getName() { return name; };
    std::vector<FunctionInfo *> getFuncs() { return funcs; };

    int addFunctionInfo(FunctionInfo *func);
};

class BinaryInfo {
private:
    std::string name;
    std::vector<LibraryInfo *> libs;
    std::string logStr;

public:
    BinaryInfo(std::string filename);

    // Add a library's stats to the binary's total statistics
    int addLibraryInfo(LibraryInfo *lib);
    int writeToJSON(std::string path);
    void setLogStr(std::string msg);
};

#endif
