#include "libfilter.h"
#include <assert.h>
#include <cstring>  // for std::strcmp
#include <fstream>
#include <functional>
#include <iostream>

#include "chunk/dump.h"
#include "conductor/conductor.h"
#include "log/registry.h"
#include "log/temp.h"
#include "pass/chunkpass.h"

#include "callgraph.h"
#include "erased_stats.h"
#include "indirect_fcg.h"
#include "static_fcg.h"
#include "sysfilter.h"

int Libfilter::parse(int argc, char **argv) {
    cxxopts::Options opts("libfilter_extract",
        "Tool to statically determine unused functions in the "
        "libraries of executables");

    try {
        buildOptions(opts);
        extractConfig(opts, argc, argv);
        return parse();
    }
    catch (std::exception &e) {
        printUsage();
        const char *message = e.what();
        std::cout << "Exception: " << message << std::endl;
        if (!config.outputJsonPath.empty()) {
            BinaryInfo *binInfo = new BinaryInfo(config.mainExecName);
            binInfo->setLogStr(std::string(message));
            binInfo->writeToJSON(config.outputJsonPath);
        }
    }
    return 1;
}

int Libfilter::parse() {
    try {
        if (config.outputFile == "") {
            config.outputStream = &std::cout;
        }
        else {
            config.outputStream = new std::ofstream(config.outputFile.c_str());
            if (!(*config.outputStream)) {
                throw std::runtime_error("Couldn't open output file");
            }
        }

        if (ElfMap::isElf(config.mainExecPath_c_str)) {
            // Record the basename of the executable whose libraries we'll be
            // analyzing
            BinaryInfo *binInfo = new BinaryInfo(config.mainExecName);

            // Now have sysfilter parse the ELF
            int errcde;
            if ((errcde = sf.parse(config.sysfilterArgs.size(),
                     config.sysfilterArgs.data())) != 0) {
                throw std::runtime_error(
                    "Sysfilter parsing returned an error but didn't throw "
                    "exception");
            }

            if ((errcde = sf.run()) != 0) {
                throw std::runtime_error(
                    "Sysfilter run returned an error but didn't throw "
                    "exception");
            }

            // Get the set of reachable functions from the program's entry
            // points
            auto reachableFuncs = sf.getReachableFunctions();

            // Get the list of libraries used by the program
            setup = sf.getSetup();
            auto liblist = setup->getConductor()->getLibraryList();

            if (!liblist) {
                throw std::runtime_error("No libraries present.");
            }

            // Go through the libraries
            for (auto library : CIter::children(liblist)) {
                // Emit the name of the library we'll be analyzing and begin
                // gathering stats
                (*config.outputStream)
                    << library->getModule()->getName() << std::endl;
                LibraryInfo *libInfo = new LibraryInfo(library);
                auto exclude_library = libInExcludeList(library->getName());

                // Go through the functions of this library
                auto funclist = library->getModule()->getFunctionList();
                for (auto func : CIter::children(funclist)) {
                    // Set up function information
                    FunctionInfo *funcInfo = new FunctionInfo(func);
                    auto do_not_erase = exclude_library ||
                                        functionInExcludeList(
                                            library->getName(),
                                            func->getName());

                    // Determine if the function needs to be marked as
                    // eraseable or not
                    if ((!do_not_erase) &&
                        (reachableFuncs.find(func) == reachableFuncs.end())) {
                        // The function we're examining is not explicity
                        // included in the reachable set. However, it could
                        // still overlap with a function in the reachable set
                        // instead (in which case we shouldn't consider
                        // it eraseable)
                        bool overlap = false;
                        for (auto reachable_func : reachableFuncs) {
                            // Check that the parent of the reachable function
                            // is in the module whose function list we're
                            // going though
                            auto rf_parent = reachable_func->getParent()
                                                 ->getParent();
                            if (rf_parent != library->getModule()) {
                                continue;  // Skip if in a different library
                            }
                            // Check whether or not the ranges overlap
                            if (reachable_func->getRange().overlaps(
                                    func->getRange())) {
                                // The function we're examining overlaps with a
                                // function in the reachable set
                                overlap = true;
                                break;
                            }
                        }

                        if (!overlap) {
                            // This function is neither in the reachable set nor
                            // overlaps with any of the reachable set's
                            // functions. So we can consider the function
                            // eraseable

                            // Mark
                            funcInfo->markAsErased();
                            // Emit
                            (*config.outputStream) << func->getName() << ": ";
                            (*config.outputStream)
                                << std::hex << "0x" << func->getAddress() << " "
                                << std::dec << func->getSize() << std::endl;
                        }
                    }
                    // Record this function's information to the information
                    // we're gather for this library
                    libInfo->addFunctionInfo(funcInfo);
                }
                // Finished analyzing all of the library's functions, add to
                // the info we're gathering for the binary
                binInfo->addLibraryInfo(libInfo);
            }
            // Write down function/erasure info to JSON
            if (!config.outputJsonPath.empty()) {
                binInfo->writeToJSON(config.outputJsonPath);
            }
        }
        else {
            throw std::runtime_error("Non-ELF file given");
            // setup->parseEgalitoArchive(config.mainExecPath_c_str);
        }
    }
    catch (std::exception &e) {
        const char *message = e.what();
        std::cerr << "Exception: " << message << std::endl;
        if (!config.outputJsonPath.empty()) {
            BinaryInfo *binInfo = new BinaryInfo(config.mainExecName);
            binInfo->setLogStr(std::string(message));
            binInfo->writeToJSON(config.outputJsonPath);
        }

        return 1;
    }
    catch (const char *message) {
        std::cout << "Exception: " << message << std::endl;
        if (!config.outputJsonPath.empty()) {
            BinaryInfo *binInfo = new BinaryInfo(config.mainExecName);
            binInfo->setLogStr(std::string(message));
            binInfo->writeToJSON(config.outputJsonPath);
        }
        return 1;
    }
    return 0;
}

void Libfilter::buildOptions(cxxopts::Options &opts) {
    opts.add_options("Internal")(ExtractOpts::OPT_MAIN_EXEC,
        "Executable file whose libraries to analyze",
        cxxopts::value<std::string>());

    auto help_opt = ExtractOpts::OPT_HELP_SHORT + "," + ExtractOpts::OPT_HELP;
    auto verbose_opt = ExtractOpts::OPT_VERBOSE_SHORT + "," +
                       ExtractOpts::OPT_VERBOSE;
    opts.add_options(ExtractOpts::OPTS_GENERAL)(help_opt, "This usage message")(
        ExtractOpts::OPT_DL_FILE,
        "JSON file describing dlopen() and dlsym() use.",
        cxxopts::value<std::vector<std::string>>())(ExtractOpts::OPT_EXTRA_DEP,
        "Parse extra libraries added with '--dl-file' as dependencies of main "
        "binary. (EXPERIMENTAL)")(verbose_opt, "Be more verbose");

    auto output_opt = ExtractOpts::OPT_OUTPUT_SHORT + "," +
                      ExtractOpts::OPT_OUTPUT;
    auto json_opt = ExtractOpts::OPT_JSON_SHORT + "," + ExtractOpts::OPT_JSON;
    opts.add_options(ExtractOpts::OPTS_OUTPUT)(output_opt,
        "Path to output results",
        cxxopts::value<std::string>()->default_value(""))(json_opt,
        "Path for JSON FCG erasure stats",
        cxxopts::value<std::string>()->default_value(""));

    auto exclude_opt = ExtractOpts::OPT_EXCLUDE_SHORT + "," +
                       ExtractOpts::OPT_EXCLUDE;
    opts.add_options(ExtractOpts::OPTS_EXCLUDE)(exclude_opt,
        "Path to file containing newline-separated list of libraries to ignore "
        "when erasing",
        cxxopts::value<std::string>()->default_value(""));

    opts.parse_positional(ExtractOpts::OPT_MAIN_EXEC);
    opts.positional_help(ExtractOpts::OPT_MAIN_EXEC);

    helpstr = opts.help({ExtractOpts::OPTS_GENERAL, ExtractOpts::OPTS_OUTPUT,
        ExtractOpts::OPTS_EXCLUDE});
}

void Libfilter::extractConfig(cxxopts::Options opts, int argc, char **argv) {
    using namespace ExtractOpts;

    auto copt = opts.parse(argc, argv);
    config.sysfilterArgs = std::vector<char *>{argv[0]};

    // First parse flags
    if (copt[ExtractOpts::OPT_HELP].as<bool>()) {
        throw std::runtime_error("Help requested, so not doing any work.");
    }

    if (copt.count(ExtractOpts::OPT_JSON) > 1) {
        throw std::runtime_error("Multiple output JSON paths given!");
    }
    if (copt.count(ExtractOpts::OPT_JSON) == 1) {
        config.outputJsonPath = copt[ExtractOpts::OPT_JSON].as<std::string>();
        // Check that the json can be opened
        std::ofstream outfile;
        outfile.open(config.outputJsonPath);
        if (!outfile.is_open()) {
            throw std::runtime_error("Error opening path to JSON.");
        }
        outfile.close();

        config.sysfilterArgs.push_back("-j");
        config.sysfilterArgs.push_back((char *)config.outputJsonPath.c_str());
    }

    config.outputFile = copt[ExtractOpts::OPT_OUTPUT].as<std::string>();
    config.sysfilterArgs.push_back("-o");
    config.sysfilterArgs.push_back((char *)config.outputFile.c_str());

    if (copt.count(ExtractOpts::OPT_EXCLUDE) > 1) {
        throw std::runtime_error("Multiple exclude file paths given!");
    }
    if (copt.count(ExtractOpts::OPT_EXCLUDE) == 1) {
        auto excludeFile = copt[ExtractOpts::OPT_EXCLUDE].as<std::string>();
        std::ifstream exclude_stream(excludeFile);
        std::string entry;
        while (getline(exclude_stream, entry)) {
            auto div = entry.find(":");
            auto lib_name = entry.substr(0, div);
            if (div == std::string::npos) {
                config.excludeLibraries.insert(lib_name);
            }
            else {
                auto function_name = entry.substr(div + 1);
                config.excludeFunctions[lib_name].insert(function_name);
            }
        }
        exclude_stream.close();
    }

    // Now parse positional arguments
    if (copt.count(ExtractOpts::OPT_MAIN_EXEC) == 0) {
        throw std::runtime_error("No input file given.");
    }
    else if (copt.count(ExtractOpts::OPT_MAIN_EXEC) > 1) {
        throw std::runtime_error("Multiple input files given!");
    }
    config.mainExecPath = copt[ExtractOpts::OPT_MAIN_EXEC].as<std::string>();
    config.sysfilterArgs.push_back((char *)config.mainExecPath.c_str());

    // Make a copy of the main exec path for use in an argv[] (which does
    // not accept the const char * provided by std::string.c_str()
    auto exec_cstr_size = config.mainExecPath.size() + 1;

    config.mainExecPath_c_str = (char *)std::malloc(exec_cstr_size);
    std::strncpy(
        config.mainExecPath_c_str, config.mainExecPath.c_str(), exec_cstr_size);

    // Extract the basename of the main exec from the path
    std::string filename(config.mainExecPath);
    const size_t last_slash_idx = filename.find_last_of("\\/");
    if (std::string::npos != last_slash_idx) {
        filename.erase(0, last_slash_idx + 1);
    }
    config.mainExecName = filename;
}

void Libfilter::printUsage() {
    std::cout << "Libfilter extraction tool" << std::endl;
    std::cout << helpstr << std::endl;
}

std::string Libfilter::getMainExecPath() {
    return config.mainExecPath;
}

std::string Libfilter::getOutputFilePath() {
    return config.outputFile;
}

std::vector<std::string> Libfilter::getLibraryPaths() {
    if (setup == nullptr) {
        throw std::runtime_error("Tried to use extract tool before parse");
    }

    auto liblist = setup->getConductor()->getLibraryList();
    if (!liblist) {
        throw std::runtime_error("No libraries present.");
    }

    std::vector<std::string> path_list;
    for (auto library : CIter::children(liblist)) {
        path_list.push_back(library->getResolvedPath());
    }
    return path_list;
}

bool Libfilter::libInExcludeList(std::string library_path) {
    return config.excludeLibraries.count(library_path) > 0;
}

bool Libfilter::functionInExcludeList(
    std::string library_name, std::string function_name) {
    return config.excludeFunctions[library_name].count(function_name) > 0;
}
