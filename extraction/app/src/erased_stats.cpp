#include <fstream>
#include "erased_stats.h"
#include "json.hpp"

// Binary Info ================================================================
BinaryInfo::BinaryInfo(std::string filename) {
    name = filename;
}

int BinaryInfo::addLibraryInfo(LibraryInfo *lib) {
    libs.push_back(lib);
    return 0;
}

void BinaryInfo::setLogStr(std::string msg) {
    logStr = msg;
}

int BinaryInfo::writeToJSON(std::string path) {
    std::ofstream outfile;
    outfile.open(path);

    nlohmann::json jbin;
    jbin["binName"] = name;
    jbin["log_str"] = logStr;

    // Iterate thru libs
    nlohmann::json jlibs;
    for (std::vector<LibraryInfo *>::iterator l = libs.begin(); l != libs.end();
         ++l) {
        LibraryInfo *linfo = *l;
        // Iterate thru all funcs
        nlohmann::json jfuncs;
        std::vector<FunctionInfo *> funcs = linfo->getFuncs();
        for (std::vector<FunctionInfo *>::iterator f = funcs.begin();
             f != funcs.end(); ++f) {
            FunctionInfo *finfo = *f;
            jfuncs += {{"funcName", finfo->getName()},
                {"offset", finfo->getOffset()}, {"size", finfo->getSize()},
                {"erased", finfo->isErased()}};
        }
        // Record what we've collected to the json for the library
        jlibs["libName"] = linfo->getName();
        jlibs["funcs"] = jfuncs;

        // Add the library json to the bin json
        jbin["libs"] += jlibs;
    }

    outfile << jbin.dump(2) << std::endl;
    outfile.close();
    return 0;
}

// Library Info ===============================================================
LibraryInfo::LibraryInfo(Library *lib) {
    name = lib->getModule()->getName();
}

int LibraryInfo::addFunctionInfo(FunctionInfo *func) {
    funcs.push_back(func);
    return 0;
}

// Function Info ==============================================================
FunctionInfo::FunctionInfo(Function *func) {
    name = func->getName();
    size = func->getSize();
    offset = func->getAddress();
    erased = false;
}
