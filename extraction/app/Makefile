# Makefile for egalito applications

# Default target
.PHONY: all
all: app-all .symlinks
	@true


ifdef SYSTEM_LIBS
BUILDDIR?=build_x86_64/
SHORT_CXX=@echo "CXX $<"; $(CXX)
SHORT_LINK=@echo "LINK $<"; $(CXX)

CFLAGS      += -g -I /usr/include/sysfilter $(shell pkg-config --cflags egalito)
CXXFLAGS    += -g -I /usr/include/sysfilter $(shell pkg-config --cflags egalito)
CLDFLAGS    += -lsysfilter -legalito -lcapstone

$(BUILDDIR)%.o: %.cpp
	$(SHORT_CXX) $(CXXFLAGS) $(DEPFLAGS) -c -o $@ $<

else
include ../sysfilter/extraction/egalito/env.mk
include ../sysfilter/.version-info.mk

CFLAGS      += -g -I ../sysfilter/extraction/egalito/src/ -I ../sysfilter/extraction/app/src/ $(SYSFILTER_VERSION_FLAGS)
CXXFLAGS    += -g -I ../sysfilter/extraction/egalito/src/ -I ../sysfilter/extraction/app/src/ $(SYSFILTER_VERSION_FLAGS)
CLDFLAGS    += -L ../sysfilter/extraction/egalito/src/$(BUILDDIR) -legalito \
	-Wl,-rpath=$(abspath ../sysfilter/extraction/egalito/src/$(BUILDDIR)) \
	-Wl,-rpath=$(abspath ../sysfilter/extraction/egalito/dep/capstone/install/lib)\
	-Wl,-rpath=$(abspath ../sysfilter/extraction/egalito/src)

app-all: | rebuild-src

ifeq ($(findstring clean,$(MAKECMDGOALS)),)
Makefile: rebuild-src
endif
APP_SOURCES = $(wildcard ../sysfilter/extraction/app/src/*.cpp)
endif

ifdef ENABLE_COVERAGE
CFLAGS 		+= -fprofile-arcs  -ftest-coverage
CXXFLAGS 	+= -fprofile-arcs  -ftest-coverage
CLDFLAGS 	+= -lgcov
endif

ETAPP = $(BUILDDIR)libfilter_extract
APP_SOURCES += $(wildcard src/*.cpp)
exe-filename = $(foreach s,$1,$(BUILDDIR)$(dir $s)$(basename $(notdir $s)))
obj-filename = $(foreach s,$1,$(BUILDDIR)$(dir $s)$(basename $(notdir $s)).o)
dep-filename = $(foreach s,$1,$(BUILDDIR)$(dir $s)$(basename $(notdir $s)).d)

ETAPP_SOURCES = $(APP_SOURCES)
ETAPP_OBJECTS = $(call obj-filename,$(ETAPP_SOURCES))

ALL_SOURCES = $(sort $(ETAPP_SOURCES))
ALL_OBJECTS = $(call obj-filename,$(ALL_SOURCES))

BUILDTREE = $(sort $(dir $(ALL_OBJECTS)))

OUTPUTS = $(ETAPP)

app-all: $(OUTPUTS)

$(ALL_OBJECTS): | $(BUILDTREE)
$(BUILDTREE):
	@mkdir -p $@

ifndef SYSTEM_LIBS
$(BUILDTREE): ../sysfilter/extraction/egalito/config/config.h

../sysfilter/extraction/egalito/config/config.h:
	$(call short-make,../sysfilter/extraction/egalito/config)
.PHONY: rebuild-src
rebuild-src:
	$(call short-make,../sysfilter/extraction/egalito/src)
endif

.symlinks: $(OUTPUTS)
	@touch .symlinks
	@echo "LN-S" $(OUTPUTS)
	@ln -sf $(ETAPP)
ifndef SYSTEM_LIBS
	@ln -sf $(shell pwd)/../sysfilter/extraction/egalito/src/$(BUILDDIR)libegalito.so $(BUILDDIR)libegalito.so
endif

# Dependencies
DEPEND_FILES = $(call dep-filename,$(ALL_SOURCES))
-include $(DEPEND_FILES)

# Special files

# Programs and libraries
$(ETAPP): $(ETAPP_OBJECTS)
	$(SHORT_LINK) $(CXXFLAGS) -o $@ $^ $(CLDFLAGS) -lreadline
ifndef SYSTEM_LIBS
$(ETAPP): ../sysfilter/extraction/egalito/src/$(BUILDDIR)libegalito.so
endif
# Other targets
.PHONY: clean realclean
clean:
	-rm -rf $(BUILDDIR) .symlinks etapp
