# Libfilter FCG extraction tool

This directory contains source for the libfilter's analysis tool.
Given a target program, it uses static analysis techniques to
over-approximate the program's function call graph (FCG) and from
there identify unused functions calls in the program and its shared
libraries. Output is provided as a JSON file containing the set of
syscalls invoked by the program. The extraction tool is built using
the [Egalito](https://gitlab.com/Egalito/egalito) framework.

For more details on the program's operation, including the different
callgraph extraction methods, please see our [paper](../doc/libfilter_dtrap2020.pdf).

## Cloning this repository

To clone this repository, please see the instructions
[here](../README.md).

## Requirements

Compiling the extraction tool requires the following components:
 - A modern `g++`
 - `gdb`
 - Debug symbols for glibc
 - Development headers for `readline`

Our tool is built and tested primarily on Debian [`sid`](https://www.debian.org/releases/sid/).
We have also tested the tool on [Debian Buster](https://wiki.debian.org/DebianBuster) with good results.

On Debian `sid` and Buster, you can install these dependencies as
follows:
```
sudo apt-get intall build-essential gdb libc6-dbg libreadline-dev
```

On other distributions, the required package names may vary.

# Compilation

To compile the tool on `x86-64`, run the following from the
`extraction` directory of the repository:
```
$ make
```

Parallel builds are supported, and in fact highly recommended due to
the codebase size.

# Usage

The built output executable is present in
`app/build_x86_64/libfilter_extract`. Either run this binary directly
or copy it to your `$PATH`. No additional files are needed.

An example workflow would look like the following. For this example
and all following, the working directory should be the `extraction`
directory of the repository.

```
$ tee test.c <<EOF
#include <stdio.h>
int main(void) {
    printf("Hello world!\n");
    return 0;
}
EOF

$ gcc -g test.c -o test
$ app/libfilter_extract -o test.txt test

$ cat hello.txt
module-(executable)
module-libc.so.6
check_one_fd: 0x26e70 163
__gcc_personality_v0.cold: 0x253c3 61
free_derivation: 0x16e630 177
free_modules_db: 0x16e6f0 73
free_mem: 0x16e740 93
free_mem: 0x16e7a0 33
free_mem: 0x16e7d0 53
do_release_all: 0x16e810 27
free_mem: 0x16e830 39
new_composite_name: 0x31660 507
set_binding_values.part.0: 0x34b00 1108
free_mem: 0x16ea70 173
__bsd_signal.part.0: 0x3ba60 158
__sysv_signal.part.0: 0x3c880 129
free_mem: 0x16ec40 44
strfromf.cold: 0x25623 10
strfromd.cold: 0x2562d 10
strfroml.cold: 0x25637 10
. . .
```

Full usage information can be found by running `libfilter_extract --help`.
Useful options are:

* `--output-file/-o <filename>`: File to output an "erasure file"
  indicating function addresses and offsets that are unused and can be erased
* `--dl-file <filename>`: Specify a file to use as a dynamically-loaded code
  specification. See below for example.
* `--verbose/-v`: Be more verbose. Can be repeated multiple times for more
  information as needed.
* `--json-stats/-j <filename>`: File to output all identified
  functions in each library and their erasure status (Warning:  output
  can be very large)

## Finding symbols

The analysis tool leverages debug symbol information to extract the
program's FCG.  Where symbol information is not available, sysfilter
will try to use other available information to extract function
boundaries.
For the most precise analysis, we recommend building programs with
symbol information (_e.g.,_ build your program with `-g`).  To analyze
binaries provided in your distribution, you will likely need to
install separate debug packages that provide symbol information for
the package you want to analyze,
and their dependencies.

On Debian, this involves adding a debug repository alongside the
standard repository configuration.  More information can be found at
https://wiki.debian.org/HowToGetABacktrace

For Debian `sid`, this would involve adding the following to your
`/etc/apt/sources.list`:

```
deb http://deb.debian.org/debian-debug/ sid-debug main contrib non-free
```

Once you add this repository, you should see symbol packages for most
repository packages.

Mapping system binaries to packages and their corresponding symbol
packages is not a completely straightforward process.  In general, on
Debian, a package `pkg` usually has a corresponding package named
`pkg-dbgsym` or `pkg-dbg` containing
its debug symbols.  There are some exceptions to this rule.

We have provided a script `pkgs-with-missing-symbols.sh` in the
`sysfilter/extraction/utils` directory to help identify the packages associated with a
given binary.  From there, you can search apt to find the
corresponding debug packages.

For example, for `fdisk`, we could run (from the `extraction` directory):
```
$ sudo apt-date update # Update the repository list, if you have not one so already
$ sysfilter/extraction/utils/pkgs-with-missing-symbols.sh /usr/sbin/fdisk
Packages requiring installation of debug packages:
 fdisk libfdisk1 libsmartcols1

 Depending on your distribution, debug packages usually take the form
 pkgname-dbgsym or pkgname-dbg.  Exceptions may exist.  Consult your
 package manager (_e.g.,_ apt-cache search <pkgname>) or distribution
 documentation to find the package names.
```

This script finds all shared libraries used by `fdisk` and looks up
their corresponding packages.  From here, we can search apt to find
the corresponding debug packages:

```
$ apt-cache search fdisk
fdisk - collection of partitioning utilities
libfdisk1 - fdisk partitioning library
fdisk-dbgsym - debug symbols for fdisk
libfdisk1-dbgsym - debug symbols for libfdisk1
. . .

$ apt-cache search libsmartcols1
libsmartcols1 - smart column output alignment library
libsmartcols1-dbgsym - debug symbols for libsmartcols1
```

Since we know the debug packages for each library, so we can
install each of them:
```
$ sudo apt-get install fdisk-dbgsym libfdisk1-dbgsym libsmartcols1-dbgsym
```

In some cases, the name of the debug package may not be immediately
obvious using this process, or may not be available in the
repository.  For these cases, we recommend checking your
distribution's packaging website, such as
[https://packages.debian.org](https://packages.debian.org) for more
information.

Note that Debian `sid` has the largest number of debug symbol
packages, covering most of the repository--other Debian versions or
distributions may not provide as complete coverage.


## Dynamically-loaded objects
```
    [{
        "path": "~/heap-page-stats.so",
        "symbols": ["malloc", "free", "calloc", "realloc"]
    }]
```

This file, when passed to the extraction tool via `--dl-file`, will cause the
tool to treat the file `~/heap-page-stats.so` as a dynamically-loaded
dependency, where the exported symbols `malloc`, `free`, `calloc`, and
`realloc` are imported by the program. Constructors and destructors of the
given file will also be considered as imported, just as if it was specified in
the analyzed files' `.dynamic` section.

Automatic generation of dynamic load configuration is ongoing work.

## Parse overrides

Rarely, the [Egalito](https://gitlab.com/Egalito/egalito) framework may
encounter issues disassembling certain hand-crafted assembly functions.
In these cases, the tool may crash and require a manual override to
determine function boundaries.

We have included a few parse overrides for libraries in Debian `sid` in
the `sysfilter/extraction/overrides` directory.  You can invoke the
tool to use the override files by setting `EGALITO_PARSE_OVERRIDES` to
a directory with files containing one override per library.  For
example, to use
the provided parse overrides (from this directory):

```
EGALITO_PARSE_OVERRIDES=sysfilter/extraction/overrides app/libfilter_extract -o test.txt test
```

You can find examples of parse override files in the `overrides`
directory.

## Frequently asked questions

### When I clone this directory, I get `Permission denied (pubkey)`.

The extraction tool uses several git submodules which are specified
via SSH, which requires you have active [Gitlab](https://www.gitlab.com)
__and__ [Github](https://www.github.com) accounts with valid public keys.
See the main [README](../README.md) for more details.

### The option '-v' produces a lot of output.  How can I filter it?

Logging is controlled by the [Egalito](https://gitlab.com/Egalito/egalito) framework.
You can find more information about `Egalito`'s logging in its
[tutorial](https://gitlab.com/Egalito/egalito-docs/-/blob/master/tutorial.rst).
